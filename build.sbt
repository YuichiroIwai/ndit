name := "ndit"

scalaVersion := "2.11.8"

version := "0.1.0"

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)