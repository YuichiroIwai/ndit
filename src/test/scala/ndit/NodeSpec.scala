package ndit

import org.scalatest.{FlatSpec, Matchers}

class NodeSpec extends FlatSpec with Matchers {

  behavior of "A Node"

  it can "connect other Node" in {
    val g = new Node() {} connectTo new Node() {}
    g shouldBe a[Graph]
    g.size shouldBe 2
    g.heads.size shouldBe 1
    g.leafs.size shouldBe 1
  }
}
