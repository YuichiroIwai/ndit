package ndit

trait Node {
  def connectTo(target: Node): Graph = new Graph {}
}

trait Graph {
  val size = 2
  val heads = Seq(1)
  val leafs = Seq(1)
}
